package org.bitbucket.sergey_ivanenko.task6.presentation.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLngBounds
import com.google.maps.android.clustering.ClusterManager
import org.bitbucket.sergey_ivanenko.task6.R
import org.bitbucket.sergey_ivanenko.task6.appComponent
import org.bitbucket.sergey_ivanenko.task6.common.Constants.CURRENT_CITY
import org.bitbucket.sergey_ivanenko.task6.databinding.ActivityMapsBinding
import org.bitbucket.sergey_ivanenko.task6.domain.model.Place
import org.bitbucket.sergey_ivanenko.task6.presentation.PlaceRenderer
import org.bitbucket.sergey_ivanenko.task6.presentation.adapter.MarkerInfoWindowAdapter
import org.bitbucket.sergey_ivanenko.task6.presentation.viewmodels.PlaceViewModel

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private var mMap: GoogleMap? = null
    private val binding by lazy { ActivityMapsBinding.inflate(layoutInflater) }
    private val placeViewModel: PlaceViewModel by viewModels {
        appComponent.placeViewModelFactory()
    }

    private var listPlace = emptyList<Place>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(binding.root)

        placeViewModel.fetchPlaceList(CURRENT_CITY)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync { googleMap ->
            placeViewModel.placeList.observe(this) {
                listPlace = it
                onMapReady(googleMap)
            }

            googleMap.setOnMapLoadedCallback {
                val bounds = LatLngBounds.builder()
                listPlace.forEach { bounds.include(it.latLng) }
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 75))
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap?.let {
            addClusteredMarkers(it)
        }
    }

    private fun addClusteredMarkers(googleMap: GoogleMap) {
        val clusterManager = ClusterManager<Place>(this, googleMap)
        clusterManager.renderer =
            PlaceRenderer(
                this,
                googleMap,
                clusterManager
            )

        clusterManager.markerCollection.setInfoWindowAdapter(MarkerInfoWindowAdapter(this))

        clusterManager.addItems(listPlace)
        clusterManager.cluster()

        googleMap.setOnCameraIdleListener {
            clusterManager.markerCollection.markers.forEach { it.alpha = 1.0f }
            clusterManager.clusterMarkerCollection.markers.forEach { it.alpha = 1.0f }

            clusterManager.onCameraIdle()
        }
    }
}