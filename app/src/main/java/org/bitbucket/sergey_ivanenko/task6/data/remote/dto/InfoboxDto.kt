package org.bitbucket.sergey_ivanenko.task6.data.remote.dto

import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName
import org.bitbucket.sergey_ivanenko.task6.domain.model.Place
import org.bitbucket.sergey_ivanenko.task6.domain.model.PlaceType

data class InfoboxDto(
    @SerializedName("info_id")
    val infoId: String,
    val area: String,
    @SerializedName("city_type")
    val cityType: String,
    val city: String,
    @SerializedName("address_type")
    val addressType: String,
    val address: String,
    val house: String,
    @SerializedName("install_place")
    val installPlace: String,
    @SerializedName("location_name_desc")
    val locationNameDesc: String,
    @SerializedName("gps_x")
    val gpsX: String,
    @SerializedName("gps_y")
    val gpsY: String,
    @SerializedName("inf_type")
    val infType: String
)

fun InfoboxDto.toPlace(): Place {
    return Place(
        id = infoId,
        cityType = cityType,
        city = city,
        addressType = addressType,
        address = address,
        house = house,
        installPlace = locationNameDesc,
        latLng = LatLng(gpsX.toDouble(), gpsY.toDouble()),
        placeType = PlaceType.INFOBOX.type
    )
}
