package org.bitbucket.sergey_ivanenko.task6.presentation.viewmodels

import android.location.Location
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.bitbucket.sergey_ivanenko.task6.data.remote.dto.toPlace
import org.bitbucket.sergey_ivanenko.task6.domain.PlaceRepository
import org.bitbucket.sergey_ivanenko.task6.domain.model.Place
import javax.inject.Inject

class PlaceViewModel @Inject constructor(private val repository: PlaceRepository) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val _placeList = MutableLiveData<List<Place>>()
    val placeList: LiveData<List<Place>> get() = _placeList

    fun fetchPlaceList(city: String) {
        compositeDisposable.add(
            Single.zip(
                repository.fetchAtmList(city),
                repository.fetchInfoboxList(city),
                repository.fetchFilialList(city)
            ) { atmList, infoboxList, filialList ->
                val list = mutableListOf<Place>()
                atmList.forEach { list.add(it.toPlace()) }
                infoboxList.forEach { list.add(it.toPlace()) }
                filialList.forEach { list.add(it.toPlace()) }
                findClosest(list)
            }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    _placeList.value = result
                }, {
                    Log.e("TAG", "Response not successful")
                    Log.e("TAG", it.message.toString())
                })
        )
    }

    private fun findClosest(listPlace: List<Place>): List<Place> {
        val arr = floatArrayOf(1f)
        val listClosest: MutableList<Pair<Float, Place>> = mutableListOf()
        listPlace.forEach { place ->
            Location.distanceBetween(
                CURRENT_PLACE.latitude, CURRENT_PLACE.longitude,
                place.latLng.latitude, place.latLng.longitude,
                arr
            )
            listClosest.add(Pair(arr[0], place))
        }
        val placeList: MutableList<Place> = mutableListOf()

        listClosest.sortBy { it.first }
        listClosest.take(SHOW_PLACE_COUNT).map {
            it.second.distance = "${it.first}m"
            placeList.add(it.second)
        }

        return placeList
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }

    companion object {
        private val CURRENT_PLACE = LatLng(52.425163, 31.015039)
        private const val SHOW_PLACE_COUNT = 10
    }
}