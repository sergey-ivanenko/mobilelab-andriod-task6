package org.bitbucket.sergey_ivanenko.task6.domain

import io.reactivex.Single
import org.bitbucket.sergey_ivanenko.task6.data.remote.dto.AtmDto
import org.bitbucket.sergey_ivanenko.task6.data.remote.dto.FilialDto
import org.bitbucket.sergey_ivanenko.task6.data.remote.dto.InfoboxDto

interface PlaceRepository {
    fun fetchAtmList(city: String): Single<List<AtmDto>>
    fun fetchInfoboxList(city: String): Single<List<InfoboxDto>>
    fun fetchFilialList(city: String): Single<List<FilialDto>>
}