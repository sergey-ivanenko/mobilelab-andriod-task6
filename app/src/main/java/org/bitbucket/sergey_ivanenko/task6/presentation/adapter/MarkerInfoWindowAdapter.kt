package org.bitbucket.sergey_ivanenko.task6.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import org.bitbucket.sergey_ivanenko.task6.databinding.MarkerInfoContentsBinding
import org.bitbucket.sergey_ivanenko.task6.domain.model.Place

class MarkerInfoWindowAdapter(
    private val context: Context,
) : GoogleMap.InfoWindowAdapter {

    private val binding by lazy {
        MarkerInfoContentsBinding.inflate(
            LayoutInflater.from(context)
        )
    }

    override fun getInfoContents(marker: Marker): View? {

        val place = marker.tag as? Place ?: return null

        binding.apply {
            textViewTitle.text = place.title
            textViewAddress.text = place.snippet
            textViewType.text = place.placeType
        }

        return binding.root
    }

    override fun getInfoWindow(marker: Marker): View? {
        // Return null to indicate that the
        // default window (white bubble) should be used
        return null
    }
}
