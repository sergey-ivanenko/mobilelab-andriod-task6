package org.bitbucket.sergey_ivanenko.task6.data.remote.dto

import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName
import org.bitbucket.sergey_ivanenko.task6.domain.model.Place
import org.bitbucket.sergey_ivanenko.task6.domain.model.PlaceType

data class AtmDto(
    val id: String,
    val area: String,
    @SerializedName("city_type")
    val cityType: String,
    val city: String,
    @SerializedName("address_type")
    val addressType: String,
    val address: String,
    val house: String,
    @SerializedName("install_place")
    val installPlace: String,
    @SerializedName("gps_x")
    val gpsX: String,
    @SerializedName("gps_y")
    val gpsY: String,
    @SerializedName("ATM_type")
    val atmType: String
)

fun AtmDto.toPlace(): Place {
    return Place(
        id = id,
        cityType = cityType,
        city = city,
        addressType = addressType,
        address = address,
        house = house,
        installPlace = installPlace,
        latLng = LatLng(gpsX.toDouble(), gpsY.toDouble()),
        placeType = PlaceType.ATM.type
    )
}