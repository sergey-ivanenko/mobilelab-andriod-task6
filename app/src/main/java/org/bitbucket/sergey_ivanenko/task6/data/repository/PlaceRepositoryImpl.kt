package org.bitbucket.sergey_ivanenko.task6.data.repository

import io.reactivex.Single
import org.bitbucket.sergey_ivanenko.task6.data.remote.dto.AtmDto
import org.bitbucket.sergey_ivanenko.task6.data.remote.dto.FilialDto
import org.bitbucket.sergey_ivanenko.task6.data.remote.dto.InfoboxDto
import org.bitbucket.sergey_ivanenko.task6.data.remote.api.ApiService
import org.bitbucket.sergey_ivanenko.task6.domain.PlaceRepository
import javax.inject.Inject

class PlaceRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
) : PlaceRepository {

    override fun fetchAtmList(city: String): Single<List<AtmDto>> {
        return apiService.fetchAtmList(city)
    }

    override fun fetchInfoboxList(city: String): Single<List<InfoboxDto>> {
        return apiService.fetchInfoboxList(city)
    }

    override fun fetchFilialList(city: String): Single<List<FilialDto>> {
        return apiService.fetchFilialList(city)
    }
}