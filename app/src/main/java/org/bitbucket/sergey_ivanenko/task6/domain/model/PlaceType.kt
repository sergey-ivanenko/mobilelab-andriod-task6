package org.bitbucket.sergey_ivanenko.task6.domain.model

enum class PlaceType(val type: String) {
    ATM("Банкомат"), INFOBOX("Инфокиоск"), FILIAL("Отделение")
}
