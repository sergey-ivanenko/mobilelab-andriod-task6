package org.bitbucket.sergey_ivanenko.task6.common

object Constants {
    const val BASE_URL = "https://belarusbank.by/api/"
    const val CURRENT_CITY = "Гомель"
}