package org.bitbucket.sergey_ivanenko.task6.domain.model

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

data class Place(
    val id: String,
    val cityType: String,
    val city: String,
    val addressType: String,
    val address: String,
    val house: String,
    val installPlace: String,
    val latLng: LatLng,
    val placeType: String,
    var distance: String = ""
) : ClusterItem {
    override fun getPosition(): LatLng = latLng

    override fun getTitle(): String = installPlace

    override fun getSnippet(): String = "$addressType$address $house"
}
