package org.bitbucket.sergey_ivanenko.task6.di

import dagger.Component
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.bitbucket.sergey_ivanenko.task6.common.Constants.BASE_URL
import org.bitbucket.sergey_ivanenko.task6.data.remote.api.ApiService
import org.bitbucket.sergey_ivanenko.task6.data.repository.PlaceRepositoryImpl
import org.bitbucket.sergey_ivanenko.task6.domain.PlaceRepository
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Component(modules = [AppModule::class])
@Singleton
interface AppComponent {
    fun placeViewModelFactory(): PlaceViewModelFactory
}

@Module(includes = [NetworkModule::class])
object AppModule {

    @Provides
    fun providePlaceRepository(apiService: ApiService): PlaceRepository =
        PlaceRepositoryImpl(apiService)
}

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideHttpLoginInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return httpLoggingInterceptor
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(httpLoginInterceptor: HttpLoggingInterceptor) =
        OkHttpClient.Builder()
            .addInterceptor(httpLoginInterceptor)
            .build()

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService =
        retrofit.create(ApiService::class.java)
}