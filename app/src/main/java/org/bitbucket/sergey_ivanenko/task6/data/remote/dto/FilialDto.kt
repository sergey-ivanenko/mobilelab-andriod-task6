package org.bitbucket.sergey_ivanenko.task6.data.remote.dto

import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName
import org.bitbucket.sergey_ivanenko.task6.domain.model.Place
import org.bitbucket.sergey_ivanenko.task6.domain.model.PlaceType

data class FilialDto(
    @SerializedName("filial_id")
    val filialId: String,
    @SerializedName("filial_name")
    val filialName: String,
    @SerializedName("name_type")
    val nameType: String,
    val name: String,
    @SerializedName("street_type")
    val streetType: String,
    val street: String,
    @SerializedName("home_number")
    val homeNumber: String,
    @SerializedName("info_text")
    val infoText: String,
    @SerializedName("GPS_X")
    val gpsX: String,
    @SerializedName("GPS_Y")
    val gpsY: String,
    @SerializedName("cbu_num")
    val cbuNum: String,
    @SerializedName("otd_num")
    val otdNum: String
)

fun FilialDto.toPlace(): Place {
    return Place(
        id = filialId,
        cityType = nameType,
        city = name,
        addressType = streetType,
        address = street,
        house = homeNumber,
        installPlace = infoText,
        latLng = LatLng(gpsX.toDouble(), gpsY.toDouble()),
        placeType = PlaceType.FILIAL.type
    )
}
