package org.bitbucket.sergey_ivanenko.task6.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.bitbucket.sergey_ivanenko.task6.presentation.viewmodels.PlaceViewModel
import javax.inject.Inject
import javax.inject.Provider

class PlaceViewModelFactory @Inject constructor(
    placeViewModelProvider: Provider<PlaceViewModel>,
) : ViewModelProvider.Factory {

    private val providers = mapOf<Class<*>, Provider<out ViewModel>>(
        PlaceViewModel::class.java to placeViewModelProvider
    )

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return providers[modelClass]?.get() as T
    }
}
